<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=.6">
    <title>Soft & Smart</title>
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta3/css/all.min.css">
</head>

<body>
    @include('header')

    <div class="main-container">
        <div class="marquee-container">
            <p class="marquee-title"> Old Links </a>
            <div class="marquee">
                <ul style="list-style:none;">
                    <li>
                    <span style="font-size:18px; margin-right:20px; color:white"><i class="fas fa-hand-point-right" style="color:aquamarine; margin-right:12px; padding:5px;"></i>Your Neon Text Here</span>
                    </li>
                    <li>
                    <span style="font-size:18px; margin-right:20px; color:white"><i class="fas fa-hand-point-right" style="color:aquamarine; margin-right:12px; padding:5px;"></i>Your Neon Text Here</span>
                    </li>
                    <li>
                    <span style="font-size:18px; margin-right:20px; color:white"><i class="fas fa-hand-point-right" style="color:aquamarine; margin-right:12px; padding:5px;"></i>Your Neon Text Here</span>
                    </li> <li>
                    <span style="font-size:18px; margin-right:20px; color:white"><i class="fas fa-hand-point-right" style="color:aquamarine; margin-right:12px; padding:5px;"></i>Your Neon Text Here</span>
                    </li> <li>
                    <span style="font-size:18px; margin-right:20px; color:white"><i class="fas fa-hand-point-right" style="color:aquamarine; margin-right:12px; padding:5px;"></i>Your Neon Text Here</span>
                    </li> <li>
                    <span style="font-size:18px; margin-right:20px; color:white"><i class="fas fa-hand-point-right" style="color:aquamarine; margin-right:12px; padding:5px;"></i>Your Neon Text Here</span>
                    </li> <li>
                    <span style="font-size:18px; margin-right:20px; color:white"><i class="fas fa-hand-point-right" style="color:aquamarine; margin-right:12px; padding:5px;"></i>Your Neon Text Here</span>
                    </li> <li>
                    <span style="font-size:18px; margin-right:20px; color:white"><i class="fas fa-hand-point-right" style="color:aquamarine; margin-right:12px; padding:5px;"></i>Your Neon Text Here</span>
                    </li> <li>
                    <span style="font-size:18px; margin-right:20px; color:white"><i class="fas fa-hand-point-right" style="color:aquamarine; margin-right:12px; padding:5px;"></i>Your Neon Text Here</span>
                    </li> <li>
                    <span style="font-size:18px; margin-right:20px; color:white"><i class="fas fa-hand-point-right" style="color:aquamarine; margin-right:12px; padding:5px;"></i>Your Neon Text Here</span>
                    </li>
                </ul>
            </div>
        </div>

        <div class="login-container">
            <h2>Login</h2>
            <form method="POST" action="{{ route('login') }}">
                @csrf
                <div class="input-group">
                    <label for="username">Username</label>
                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <div class="input-group">
                    <label for="password">Password</label>
                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
                <button type="submit">Login</button>
            </form>
            <br>
            <a href="/register"> <button type="button">Register</button></a>
            <!-- <div class="card-header">{{ __('Login') }}</div> -->

            <p id="errorMessage"></p>
        </div>
        <div class="marquee-container">
            <p class="marquee-title"> New Links </a>

            <div class="marquee">
                <span>Your Neon Text Here</span>
            </div>
        </div>

    </div>
    <script src="script.js"></script>
</body>
@include('footer');

</html>